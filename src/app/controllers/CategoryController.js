import { LoadingContext } from "react-router-loading";

import CategoryComponent from '../views/Category/Category'

export const preloadData = async (param) => {
    await new Promise(data => setTimeout(data, 1000));
    const data = param;
    return {
        parameters: data,
    };
};

export const Category = (props) => (
    <LoadingContext.Consumer>
        {(loadingContext) => (
            <CategoryComponent loadingContext={loadingContext} {...props} />
        )}
    </LoadingContext.Consumer>
);