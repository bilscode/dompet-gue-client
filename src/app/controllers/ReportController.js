import { LoadingContext } from "react-router-loading";

import ReportComponent from '../views/Report/Report'

export const preloadData = async (param) => {
    await new Promise(data => setTimeout(data, 1000));
    const data = param;
    return {
        parameters: data,
    };
};

export const Report = (props) => (
    <LoadingContext.Consumer>
        {(loadingContext) => (
            <ReportComponent loadingContext={loadingContext} {...props} />
        )}
    </LoadingContext.Consumer>
);