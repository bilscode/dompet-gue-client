import { LoadingContext } from "react-router-loading";

import DashboardComponent from '../views/Dashboard/Dashboard'

export const preloadData = async (param) => {
    await new Promise(data => setTimeout(data, 1000));
    const data = param;
    return {
        parameters: data,
        wallet: [
            {   
                id: 1,
                name: 'Total',
                balance: '12.000.000',
                currency: 'Rp',
                icon: 'dompet-gue-icon/package.png'
            },
            {   
                id: 3,
                name: 'Dompet Gue',
                balance: '12.000.000',
                currency: 'Rp',
                icon: 'dompet-gue-icon/wallet.png'
            }
        ],
    };
};

export const Dashboard = (props) => (
    <LoadingContext.Consumer>
        {(loadingContext) => (
            <DashboardComponent loadingContext={loadingContext} {...props} />
        )}
    </LoadingContext.Consumer>
);