import { LoadingContext } from "react-router-loading";

import ProfileComponent from '../views/Profile/Profile'

export const preloadData = async (param) => {
    await new Promise(data => setTimeout(data, 1000));
    const data = param;
    return {
        parameters: data,
    };
};

export const Profile = (props) => (
    <LoadingContext.Consumer>
        {(loadingContext) => (
            <ProfileComponent loadingContext={loadingContext} {...props} />
        )}
    </LoadingContext.Consumer>
);