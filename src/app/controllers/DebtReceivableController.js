import { LoadingContext } from "react-router-loading";

import DebtReceivableComponent from '../views/DebtReceivable/DebtReceivable'

export const preloadData = async (param) => {
    await new Promise(data => setTimeout(data, 1000));
    const data = param;
    return {
        parameters: data,
    };
};

export const DebtReceivable = (props) => (
    <LoadingContext.Consumer>
        {(loadingContext) => (
            <DebtReceivableComponent loadingContext={loadingContext} {...props} />
        )}
    </LoadingContext.Consumer>
);