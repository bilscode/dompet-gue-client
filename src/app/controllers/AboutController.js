import { LoadingContext } from "react-router-loading";

import AboutComponent from '../views/About/About'

export const preloadData = async (param) => {
    await new Promise(data => setTimeout(data, 1000));
    const data = param;
    return {
        parameters: data,
    };
};

export const About = (props) => (
    <LoadingContext.Consumer>
        {(loadingContext) => (
            <AboutComponent loadingContext={loadingContext} {...props} />
        )}
    </LoadingContext.Consumer>
);