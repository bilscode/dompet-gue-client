import { LoadingContext } from "react-router-loading";

import ScheduleTransactionComponent from '../views/ScheduleTransaction/ScheduleTransaction'

export const preloadData = async (param) => {
    await new Promise(data => setTimeout(data, 1000));
    const data = param;
    return {
        parameters: data,
    };
};

export const ScheduleTransaction = (props) => (
    <LoadingContext.Consumer>
        {(loadingContext) => (
            <ScheduleTransactionComponent loadingContext={loadingContext} {...props} />
        )}
    </LoadingContext.Consumer>
);