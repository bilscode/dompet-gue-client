import { LoadingContext } from "react-router-loading";

import TransactionComponent from '../views/Transaction/Transaction'

export const preloadData = async (param) => {
    await new Promise(data => setTimeout(data, 1000));
    const data = param;
    return {
        parameters: data,
        transactions: [
            {
                id: 1
            }, 
            {
                id: 2
            }
        ]
    };
};

export const Transaction = (props) => (
    <LoadingContext.Consumer>
        {(loadingContext) => (
            <TransactionComponent loadingContext={loadingContext} {...props} />
        )}
    </LoadingContext.Consumer>
);