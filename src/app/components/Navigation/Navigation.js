import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './style.css'

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pathname: window.location.pathname
        }
        this.url = [
            {
                pathname: '/',
                name: 'Dashboard',
                icon: 'flaticon-141-home'
            },
            {
                pathname: '/transaction',
                name: 'Transaksi',
                icon: 'flaticon-036-calculator'
            },
            {
                pathname: '/report',
                name: 'Laporan',
                icon: 'flaticon-004-bar-chart'
            },
            {
                pathname: '/schedule-transaction',
                name: 'Perencanaan',
                icon: 'flaticon-163-calendar'
            },
            {
                pathname: '/debt-receivable',
                name: 'Hutang & Piutang',
                icon: 'flaticon-092-money'
            }
        ]
    }

    activeMenu = (match, location) => {
        if (match) {
            if (this.state.pathname !== location.pathname) {
                this.setState({
                    pathname: location.pathname
                })
            }
            return true
        }
        return false
    }

    render() { 
        return (
            <>
                <div className="header">
                    <div className="header-content">
                        <nav className="navbar navbar-expand">
                            <div className="collapse navbar-collapse justify-content-between">
                                <div className="header-left">
                                    <div className="d-flex align-items-center wallet-picker dropdown-toggle" role="button" data-toggle="dropdown">
                                        <img src="dompet-gue-icon/wallet.png" className="img-fluid rounded-circle mr-2 wallet-icon"/>
                                        <div className="mr-2">
                                            <div className="m-0 text-muted">DOMPET GUE</div>
                                            <div className="text-primary font-weight-bold">Rp 120.000.000</div>
                                        </div>
                                    </div>
                                    <div className="dropdown-menu">
                                        <h5 className="dropdown-header">Pilih Dompet</h5>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item p-2" href="#">
                                            <div className="d-flex align-items-center wallet-picker">
                                                <img src="dompet-gue-icon/wallet.png" className="img-fluid rounded-circle mr-3 wallet-icon"/>
                                                <div className="pr-5 mr-5">
                                                    <div className="m-0">DOMPET GUE</div>
                                                    <div className="text-primary font-weight-bold">Rp 120.000.000</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item p-2" href="#">
                                            <div className="d-flex align-items-center wallet-picker">
                                                <img src="dompet-gue-icon/wallet.png" className="img-fluid rounded-circle mr-3 wallet-icon"/>
                                                <div className="pr-5 mr-5">
                                                    <div className="m-0">DOMPET GUE</div>
                                                    <div className="text-primary font-weight-bold">Rp 120.000.000</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item p-2" href="#">
                                            <div className="d-flex align-items-center wallet-picker">
                                                <img src="dompet-gue-icon/wallet.png" className="img-fluid rounded-circle mr-3 wallet-icon"/>
                                                <div className="pr-5 mr-5">
                                                    <div className="m-0">DOMPET GUE</div>
                                                    <div className="text-primary font-weight-bold">Rp 120.000.000</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <ul className="navbar-nav header-right main-notification">
                                    <li className="nav-item dropdown header-profile">
                                        <a className="nav-link" href="#" role="button" data-toggle="dropdown">
                                            <img src="images/profile/pic1.jpg" width="20" className="img-fluid rounded-circle" />
                                            <div className="header-info">
                                                <span>Billisany</span>
                                                <small>billisanyakhyar.testing@gmail.com</small>
                                            </div>
                                        </a>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <Link to="/profile" className="dropdown-item ai-icon">
                                                <i className="text-primary fa fa-user"></i>    
                                                <span className="ml-2">Profil </span>
                                            </Link>
                                            <hr className="my-1"/>
                                            <Link to="/dompet-gue" className="dropdown-item ai-icon">
                                                <i className="text-primary fas fa-wallet"></i>    
                                                <span className="ml-2">Dompet Gue </span>
                                            </Link>
                                            <Link to="/category" className="dropdown-item ai-icon">
                                                <i className="text-primary fas fa-list"></i>      
                                                <span className="ml-2">Kategori </span>
                                            </Link>
                                            <Link to="/about" className="dropdown-item ai-icon">
                                                <i className="text-primary fas fa-exclamation-circle"></i>        
                                                <span className="ml-2">Tentang </span>
                                            </Link>
                                            <hr className="my-1"/>
                                            <Link to="/" className="dropdown-item ai-icon">
                                                <i className="text-danger fas fa-sign-out-alt"></i>
                                                <span className="ml-2">Logout </span>
                                            </Link>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
    
                <div className="deznav">
                    <div className="deznav-scroll">
                        <div className="main-profile">
                            <img src="images/Untitled-1.jpg" className="img-fluid rounded-circle"/>
                            <h5 className="mb-0 fs-20 text-black "><span className="font-w400">Hello,</span> Billisany</h5>
                            <p className="mb-0 fs-14 font-w400">billisanyakhyar.testing@gmail.com</p>
                        </div>
    
                        <ul className="metismenu" id="menu">
                            {this.url.map((data, index) => {
                                return (
                                    <li className={this.state.pathname == data.pathname ? 'mm-active' : '' } key={index}>
                                        <NavLink exact to={data.pathname} isActive={
                                            (match, location) => {
                                                this.activeMenu(match, location)
                                            }
                                        } className="ai-icon">
                                            <i className={data.icon}></i>
                                            <span className="nav-text">{data.name}</span>
                                        </NavLink>
                                    </li>
                                )
                            })}
                        </ul>
    
                        <div className="copyright">
                            <p><strong>{ window.app_info.app_name }</strong> © { window.app_info.year } All Rights Reserved</p>
                            <p className="fs-12">Made with <span className="heart"></span> by { window.app_info.team_name }</p>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}


export default Navigation;