import React from 'react'
import { Route, topbar  } from 'react-router-loading'

topbar.config({
    barColors: {
        '0': 'rgba(45, 184, 76, 1)',
        '.3': 'rgba(45, 184, 76, 1)',
        '1.0': 'rgba(45, 184, 76, 17)'
    },
    shadowBlur: 0
});

class FancyRoute extends React.Component {
    render () {
        return (
            <Route {...this.props}/>
        )
    }
}

export default FancyRoute