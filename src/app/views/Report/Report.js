import React, {Component} from "react";
import { preloadData } from '../../controllers/ReportController'

class Report extends Component {
    state = { 
        about: [] 
    };

    initiate = async () => {
        const data = await preloadData('test');
        this.setState({ 
            about: data
        });
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }

    render() {
        return (
        <>
            <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Laporan</h2>
                <a href="#" className="btn btn-primary mb-2"><i className="fa fa-calendar-alt mr-2"></i>Filter Periode</a>
            </div>

            <div className="card">
                <div className="card-header">
                    <h5 className="card-title">Penghasilan & Pengeluaran</h5>
                </div>
                <div className="card-body p-0">
                    <div className="custom-tab-1">
                        <ul className="nav nav-tabs w-100">
                            <li className="nav-item tabs-style-primary">
                                <a className="nav-link active py-3" data-toggle="tab" href="#this_month">BULAN INI</a>
                            </li>
                            <li className="nav-item tabs-style-primary">
                                <a className="nav-link py-3" data-toggle="tab" href="#last_month">BULAN LALU</a>
                            </li>
                        </ul>
                        <div className="tab-content p-4">
                            <div className="tab-pane fade show active" id="this_month" role="tabpanel">
                                <div className="row">
                                    <div className="col-12 col-xl-6">
                                        <h4>Saldo</h4>
                                        <h3 className="font-weight-bold">Rp 12.000.000</h3>
                                        <hr/>
                                        <div className="row">
                                            <div className="col">
                                                <p className="text-muted m-0">
                                                    Hutang
                                                </p>
                                                <h5 className="font-weight-bold text-primary">Rp 120.000</h5>
                                            </div>
                                            <div className="col">
                                                <p className="text-muted m-0">
                                                    Piutang
                                                </p>
                                                <h5 className="font-weight-bold text-primary-2">Rp 120.000</h5>
                                            </div>
                                        </div>
                                        <hr/>
                                        <p className="mb-2 text-muted">Pemasukan Bersih</p>
                                        <h5 className="font-weight-bold mb-3">Rp 12.000.000</h5>
										<img src="img/chart/bar.png" width="100%" className="mb-3"/>
                                    </div>
                                    <div className="col-12 col-xl-6">
                                        <div className="row">
                                            <div className="col">
                                                <p className="text-muted m-0">
                                                    Pemasukan
                                                </p>
                                                <h5 className="font-weight-bold text-primary">Rp 120.000</h5>
                                                <div className="text-center">
                                                    <img src="/img/chart/pie.png" width="75%" />
                                                </div>
                                            </div>
                                            <div className="col">
                                                <p className="text-muted m-0">
                                                    Pengeluaran
                                                </p>
                                                <h5 className="font-weight-bold text-primary-2">Rp 120.000</h5>
                                                <div className="text-center">
                                                    <img src="/img/chart/pie.png" width="75%" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade" id="last_month">
                            <div className="row">
                                    <div className="col-12 col-xl-6">
                                        <h4>Saldo</h4>
                                        <h3 className="font-weight-bold">Rp 12.000.000</h3>
                                        <hr/>
                                        <div className="row">
                                            <div className="col">
                                                <p className="text-muted m-0">
                                                    Hutang
                                                </p>
                                                <h5 className="font-weight-bold text-primary">Rp 120.000</h5>
                                            </div>
                                            <div className="col">
                                                <p className="text-muted m-0">
                                                    Piutang
                                                </p>
                                                <h5 className="font-weight-bold text-primary-2">Rp 120.000</h5>
                                            </div>
                                        </div>
                                        <hr/>
                                        <p className="mb-2 text-muted">Pemasukan Bersih</p>
                                        <h5 className="font-weight-bold mb-3">Rp 12.000.000</h5>
										<img src="img/chart/bar.png" width="100%" className="mb-3"/>
                                    </div>
                                    <div className="col-12 col-xl-6">
                                        <div className="row">
                                            <div className="col">
                                                <p className="text-muted m-0">
                                                    Pemasukan
                                                </p>
                                                <h5 className="font-weight-bold text-primary">Rp 120.000</h5>
                                                <div className="text-center">
                                                    <img src="/img/chart/pie.png" width="75%" />
                                                </div>
                                            </div>
                                            <div className="col">
                                                <p className="text-muted m-0">
                                                    Pengeluaran
                                                </p>
                                                <h5 className="font-weight-bold text-primary-2">Rp 120.000</h5>
                                                <div className="text-center">
                                                    <img src="/img/chart/pie.png" width="75%" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
        );
    }
}

export default Report