import React, {Component} from "react";
import { preloadData } from '../../controllers/TransactionController'

class Transaction extends Component {
    state = {
        transactions: []
    }

    initiate = async () => {
        const data = await preloadData('test');
        this.setState({ 
            transactions: data.transactions
        });
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }

    render() {
        const data_transaction = this.state.transactions.map((data, index) => {
            return (
                <div className="w-100 mb-3" key={index}>
                    <div className="d-flex">
                        <h1 className="mr-2 font-weight-bold text-black mb-0">18</h1>
                        <div> 
                            <p className="text-muted p-0 m-0" style={{ fontSize: '12px' }}>Kamis</p>
                            <p className="text-muted mb-0" style={{ fontSize: '14px', marginTop: '-4px' }}>Mei</p>
                        </div>
                        <div className="ml-auto d-flex align-items-center">
                            <h4 className="text-black font-weight-bold mb-0">Rp 10.000.000</h4>
                        </div>
                    </div>
                    <hr className="m-0"/>
                    <div className="d-flex align-items-center mt-3">
                        <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-2"/>
                        <div> 
                            <p className="text-black mb-0" style={{ fontSize: '14px' }}>Kategori</p>
                            <p className="text-muted mb-0" style={{ fontSize: '12px', marginTop: '-4px' }}>Catatan</p>
                        </div>
                        <h6 className="ml-auto text-primary mb-0">Rp 10.000.000</h6>
                    </div>
                    <div className="d-flex align-items-center mt-3">
                        <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-2"/>
                        <div> 
                            <p className="text-black mb-0" style={{ fontSize: '14px' }}>Kategori</p>
                            <p className="text-muted mb-0" style={{ fontSize: '12px', marginTop: '-4px' }}>Catatan</p>
                        </div>
                        <h6 className="ml-auto text-primary-2 mb-0">Rp 10.000.000</h6>
                    </div>
                </div>
            )
        })
            
        return (
            <>
                <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                    <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Transaksi</h2>
                    <a href="#" className="btn btn-primary mb-2"><i className="fa fa-plus mr-2"></i>Buat Transaksi</a>
                </div>
                <div className="row">
                    <div className="col-12 col-xl-4">
                        <div className="card" style={{ height: 'auto' }}>
                            <div className="card-header">
                                <h5 className="card-title">Ringkasan</h5>
                            </div>
                            <div className="card-body">
                                <div className="d-flex justify-content-between">
                                    <p className="card-text text-muted">Pemasukan</p>
                                    <p className="card-text text-primary-2 font-weight-bold">Rp 120.000.000</p>
                                </div>
                                <div className="d-flex justify-content-between">
                                    <p className="card-text text-muted">Pengeluaran</p>
                                    <p className="card-text text-primary font-weight-bold">Rp 120.000.000</p>
                                </div>
                            </div>
                            <div className="card-footer">
                                <p className="card-text text-black font-weight-bold float-right">Rp 120.000.000</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-xl-8">
                        <div className="card">
                            <div className="card-header">
                                <h5 className="card-title">Riwayat Transaksi</h5>
                            </div>
                            <div className="card-body p-0">
                                <div className="custom-tab-1">
                                    <ul className="nav nav-tabs w-100">
                                        <li className="nav-item tabs-style-primary">
                                            <a className="nav-link active py-3" data-toggle="tab" href="#this_month">BULAN INI</a>
                                        </li>
                                        <li className="nav-item tabs-style-primary">
                                            <a className="nav-link py-3" data-toggle="tab" href="#last_month">BULAN LALU</a>
                                        </li>
                                    </ul>
                                    <div className="tab-content p-3">
                                        <div className="tab-pane fade show active" id="this_month" role="tabpanel">
                                            {data_transaction}
                                        </div>
                                        <div className="tab-pane fade" id="last_month">
                                            {data_transaction}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Transaction