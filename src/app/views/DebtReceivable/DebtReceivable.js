import React, {Component} from "react";
import { preloadData } from '../../controllers/DebtReceivableController'

class DebtReceivable extends Component {
    state = {
        transactions: []
    }

    initiate = async () => {
        const data = await preloadData('test');
        this.setState({ 
            transactions: data.transactions
        });
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }

    render() {
        return (
            <>
                <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                    <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Hutang & Piutang</h2>
                    <a href="#" className="btn btn-primary mb-2"><i className="fa fa-plus mr-2"></i>Buat Hutang & Piutang</a>
                </div>

                <div className="row justify-content-center">
                    <div className="col-12 col-xl-10">
                        <div className="card">
                            <div className="card-body p-0">
                                <div className="custom-tab-1">
                                    <ul className="nav nav-tabs w-100">
                                        <li className="nav-item tabs-style-primary">
                                            <a className="nav-link active py-3" data-toggle="tab" href="#debt_tab" style={{ borderRadius: '1.375rem 0 0 0' }}>HUTANG</a>
                                        </li>
                                        <li className="nav-item tabs-style-primary">
                                            <a className="nav-link py-3" data-toggle="tab" href="#receivable_tab" style={{ borderRadius: '0 1.375rem 0 0' }}>PIUTANG</a>
                                        </li>
                                    </ul>
                                    <div className="tab-content p-3">
                                        <div className="tab-pane fade show active" id="debt_tab" role="tabpanel">
                                            <div className="d-flex align-items-center">
                                                <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                                <div> 
                                                    <p className="text-black mb-0">Billisany Akhyar</p>
                                                </div>
                                                <h6 className="ml-auto text-primary mb-0">Rp 10.000.000</h6>
                                            </div>
                                            <hr/>
                                            <div className="d-flex align-items-center">
                                                <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                                <div> 
                                                    <p className="text-black mb-0">Billisany Akhyar</p>
                                                </div>
                                                <h6 className="ml-auto text-primary mb-0">Rp 10.000.000</h6>
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="receivable_tab">
                                            <div className="d-flex align-items-center">
                                                <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                                <div> 
                                                    <p className="text-black mb-0">Billisany Akhyar</p>
                                                </div>
                                                <h6 className="ml-auto text-primary-2 mb-0">Rp 10.000.000</h6>
                                            </div>
                                            <hr/>
                                            <div className="d-flex align-items-center">
                                                <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                                <div> 
                                                    <p className="text-black mb-0">Billisany Akhyar</p>
                                                </div>
                                                <h6 className="ml-auto text-primary-2 mb-0">Rp 10.000.000</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default DebtReceivable