import React, {Component} from "react";
import { preloadData } from '../../controllers/DompetGueController'

class DompetGue extends Component {
	state = {
		wallet: []
	}
	
    initiate = async () => {
        const data = await preloadData('Billisany Akhyar');
        this.setState({
			wallet: data.wallet
		});
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }

    render() {
        const wallets = this.state.wallet.map((data, index) => {
			return (
                <div className="w-100" key={index}>
                    <div className="d-flex align-items-center py-2">
                        <img src="dompet-gue-icon/wallet.png" width="56" className="mr-3"/>
                        <div> 
                            <p className="text-black mb-0">{data.name}</p>
                            <h5 className="text-primary mb-0 font-weight-bold">Rp 120.000.000</h5>
                        </div>
                        <div className="ml-auto">
                            <a href="#" className="text-muted font-weight-bold mr-2"><i className="flaticon-039-shuffle"></i></a>
                            <a href="#" className="text-muted font-weight-bold mr-2"><i className="flaticon-003-diamond"></i></a>
                            <a href="#" className="text-muted font-weight-bold"><i className="flaticon-068-pencil"></i></a>
                        </div>
                    </div>
                    {this.state.wallet.length-1 != index && <hr/>}
                </div>
			)
		})
        return (
            <>
                <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                    <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Dompet Gue</h2>
                    <a href="#" className="btn btn-primary mb-2"><i className="fa fa-plus mr-2"></i>Buat Dompet</a>
                </div>
                
                <div className="row justify-content-center">
                    <div className="col-12 col-xl-10">
                        <div className="card">
                            <div className="card-body">
                                {wallets}
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default DompetGue