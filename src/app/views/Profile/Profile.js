import React, {Component} from "react";
import { preloadData } from '../../controllers/ProfileController'

class Profile extends Component {
    state = {
		wallet: []
	}
	
    initiate = async () => {
        const data = await preloadData('test');
        this.setState({ 
            about: data
        });
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }

    render() {
        return (
            <>
                <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                    <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Profil</h2>
                </div>
                
                <div className="row">
                    <div className="col-12 col-xl-4">
                        <div className="card" style={{ height: 'auto' }}>
                            <div className="card-body">
                                <div className="text-center">
                                    <img src="https://ui-avatars.com/api/?name=Dompet+Gue&background=2DB84C&color=fff&bold=true" width="128" className="img-fluid rounded-circle mb-4"/>
                                    <br/>
                                    <h3>Billisany Akhyar</h3>
                                    <h4 className="text-muted font-weight-normal">dompetgue.xcloudy@gmail.com</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-xl-8">
                        <div className="card">
                            <div className="card-body">
                                <div className="profile-tab">
                                    <div className="custom-tab-1">
                                        <ul className="nav nav-tabs">
                                            <li className="nav-item"><a href="#my-profile" data-toggle="tab" className="nav-link active show">Profil</a>
                                            </li>
                                            <li className="nav-item"><a href="#change-password" data-toggle="tab" className="nav-link">Ubah Kata Sandi</a>
                                            </li>
                                        </ul>
                                        <div className="tab-content">
                                            <div id="my-profile" className="tab-pane fade active show pt-4">
                                                <div className="form-group">
                                                    <label>Nama</label>
                                                    <input type="text" className="form-control" placeholder="Nama" defaultValue="Dompet Gue XCloudy"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>Email</label>
                                                    <input type="text" className="form-control" placeholder="Email" defaultValue="dompetgue.xcloudy@gmail.com" readOnly/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Mata Uang</label>
                                                    <select class="form-control default-select">
                                                        <option>IDR | Indonesia</option>
                                                        <option>IDR | Indonesia</option>
                                                        <option>IDR | Indonesia</option>
                                                    </select>
                                                </div>
                                                <div className="text-right mt-4">
                                                    <button type="button" className="btn btn-primary">
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                            <div id="change-password" className="tab-pane fade pt-4">
                                                <div className="form-group">
                                                    <label>Kata Sandi Lama</label>
                                                    <input type="password" className="form-control" placeholder="**********"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>Kata Sandi Baru</label>
                                                    <input type="password" className="form-control" placeholder="**********"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>Konfirmasi Kata Sandi</label>
                                                    <input type="password" className="form-control" placeholder="**********"/>
                                                </div>
                                                <div className="text-right mt-4">
                                                    <button type="button" className="btn btn-primary">
                                                        Simpan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Profile
