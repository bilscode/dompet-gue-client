import React, {Component} from "react";
import { preloadData } from '../../controllers/CategoryController'

class Category extends Component {
    state = {
        transactions: []
    }

    initiate = async () => {
        const data = await preloadData('test');
        this.setState({ 
            transactions: data.transactions
        });
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }

    render() {
        return (
            <>
                <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                    <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Kategori</h2>
                    <a href="#" className="btn btn-primary mb-2"><i className="fa fa-plus mr-2"></i>Buat Kategori</a>
                </div>

                <div className="row justify-content-center">
                    <div className="col-12 col-xl-10">
                        <div className="card">
                            <div className="card-body p-0">
                                <div className="custom-tab-1">
                                    <ul className="nav nav-tabs w-100">
                                        <li className="nav-item tabs-style-primary">
                                            <a className="nav-link active py-3" data-toggle="tab" href="#income_tab" style={{ borderRadius: '1.375rem 0 0 0' }}>PEMASUKAN</a>
                                        </li>
                                        <li className="nav-item tabs-style-primary">
                                            <a className="nav-link py-3" data-toggle="tab" href="#outcome_tab" style={{ borderRadius: '0 1.375rem 0 0' }}>PENGELUARAN</a>
                                        </li>
                                    </ul>
                                    <div className="tab-content p-3">
                                        <div className="tab-pane fade show active" id="income_tab" role="tabpanel">
                                            <div className="d-flex align-items-center">
                                                <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                                <p className="text-black mb-0">Kategori</p>
                                            </div>
                                            <hr/>
                                            <div className="d-flex align-items-center">
                                                <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                                <p className="text-black mb-0">Kategori</p>
                                            </div>
                                        </div>
                                        <div className="tab-pane fade" id="outcome_tab">
                                            <div className="d-flex align-items-center">
                                                <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                                <p className="text-black mb-0">Kategori</p>
                                            </div>
                                            <hr/>
                                            <div className="d-flex align-items-center">
                                                <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                                <p className="text-black mb-0">Kategori</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Category
