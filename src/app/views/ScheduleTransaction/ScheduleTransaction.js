import React, {Component} from "react";
import { preloadData } from '../../controllers/ScheduleTransactionController'

class ScheduleTransaction extends Component {
    state = {
        transactions: []
    }

    initiate = async () => {
        const data = await preloadData('test');
        this.setState({ 
            transactions: data.transactions
        });
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }

    render() {
        return (
            <>
                <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                    <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Perencanaan</h2>
                    <a href="#" className="btn btn-primary mb-2"><i className="fa fa-plus mr-2"></i>Buat Perencanaan</a>
                </div>
                <div className="row justify-content-center">
                    <div className="col-12 col-xl-10">
                        <div className="card">
                            <div className="card-body">
                                <div className="d-flex align-items-start">
                                    <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                    <div> 
                                        <p className="text-black mb-0" style={{ fontSize: '14px' }}>Kategori</p>
                                        <p className="text-muted mb-2" style={{ fontSize: '12px', marginTop: '-4px' }}>Catatan</p>
                                        <p className="text-muted mb-0" style={{ fontSize: '12px' }}>Berikutnya</p>
                                        <p className="text-muted" style={{ fontSize: '12px' }}>Sabtu, 23 Maret 2021</p>
                                    </div>
                                    <h6 className="ml-auto text-primary mb-0">Rp 10.000.000</h6>
                                </div>
                                <hr/>
                                <div className="d-flex align-items-start">
                                    <img src="dompet-gue-icon/wallet.png" width="42px" className="mr-3"/>
                                    <div> 
                                        <p className="text-black mb-0" style={{ fontSize: '14px' }}>Kategori</p>
                                        <p className="text-muted mb-2" style={{ fontSize: '12px', marginTop: '-4px' }}>Catatan</p>
                                        <p className="text-muted mb-0" style={{ fontSize: '12px' }}>Berikutnya</p>
                                        <p className="text-muted" style={{ fontSize: '12px' }}>Sabtu, 23 Maret 2021</p>
                                    </div>
                                    <h6 className="ml-auto text-primary mb-0">Rp 10.000.000</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default ScheduleTransaction