import React, {Component} from "react";
import { preloadData } from '../../controllers/AboutController'

class About extends Component {
    state = { 
        about: [] 
    };

    initiate = async () => {
        const data = await preloadData('test');
        this.setState({ 
            about: data
        });
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }

    render() {
        return (
        <>
            <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Tentang</h2>
            </div>
            <div className="row justify-content-center">
                <div className="col-12 col-xl-10">
                    <div className="card">
                        <div className="card-body">
                            <div className="text-center">
                                <img src="/logo_title.png" className="mb-3" width="30%"/>
                                <p className="text-muted">Versi 1.0 BETA</p>
                                <p className="text-black mb-0">Dikembangkan Oleh {window.app_info.team_name}</p>
                                <p className="text-black">{window.app_info.year} &copy; Hak cipta dilindungi</p>
                            </div>
                            <hr />
                            <h3 className="mb-4">Kontak</h3>
                            <div className="d-flex align-items-center mb-3">
                                <i className="fa fa-envelope mr-4 font-weight-bold" style={{ fontSize: '24px' }}></i>
                                <p className="text-black mb-0">dompetgue.xcloudy@gmail.com</p>
                            </div>
                            <div className="d-flex align-items-center mb-3">
                                <i className="fa fa-map-marker-alt mr-4 font-weight-bold text-center" style={{ fontSize: '24px', width: '24px'}}></i>
                                <p className="text-black mb-0">Jl. DI Panjaitan No.128, Karangreja Purwokerto</p>
                            </div>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3956.2735402105063!2d109.24713381532409!3d-7.4349541753147435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e655ea49d9f9885%3A0x62be0b6159700ec9!2sInstitut%20Teknologi%20Telkom%20Purwokerto!5e0!3m2!1sid!2sid!4v1622446783692!5m2!1sid!2sid" width="100%" height="300" style={{ border: 0 }} loading="lazy" className="mb-5"/>
                            
                            <h3 className="mb-4">Kontributor</h3>
                            <div className="row">
                                <div className="col-12 col-xl-4 col-md-12 text-center">
                                    <img src="img/avatar/bagus-indar.jpg" width="128" className="img-fluid rounded-circle mb-2"/>
                                    <p className="text-black m-0">Bagus Indar Suryanto</p>
                                    <p className="text-muted font-weigh-bold m-0" style={{ fontSize: '14px' }}>Frontend Developer <br/> Mobile Developer</p>
                                </div>
                                <div className="col-12 col-xl-4 col-md-12 text-center">
                                    <img src="img/avatar/billisany-akhyar.jpg" width="128" className="img-fluid rounded-circle mb-2"/>
                                    <p className="text-black m-0">Billisany Akhyar</p>
                                    <p className="text-muted font-weigh-bold m-0" style={{ fontSize: '14px' }}>Backend Developer <br/> Web Developer</p>
                                </div>
                                <div className="col-12 col-xl-4 col-md-12 text-center">
                                    <img src="img/avatar/zhafirah.jpg" width="128" className="img-fluid rounded-circle mb-2"/>
                                    <p className="text-black m-0">Zhafirah</p>
                                    <p className="text-muted font-weigh-bold m-0" style={{ fontSize: '14px' }}>Project Manager <br/> System Analyst</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
        );
    }
}

export default About
