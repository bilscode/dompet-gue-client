import React, {Component} from "react";
import { preloadData } from '../../controllers/DashboardController'

class Dashboard extends Component {
	state = {
		wallet: []
	}
	
    initiate = async () => {
        const data = await preloadData('Billisany Akhyar');
        this.setState({
			wallet: data.wallet
		});
        this.props.loadingContext.done();
    };

    componentDidMount() {
        this.initiate();
    }
	
    render() { 
		const wallets = this.state.wallet.map((data, index) => {
			return (
				<div className="swiper-slide" key={index}>
					<div className="card-bx stacked card">
						<img src={`images/card/card${data.id}.jpg`} />
						<div className="card-info">
							<p className="mb-1 text-white fs-14">{data.name}</p>
							<div className="d-flex justify-content-between">
								<h2 className="num-text text-white mb-5 font-w600">{data.currency} {data.balance}</h2>
								<img src={data.icon} width="36px" className="icon-wallet"/>
							</div>
							<div className="d-flex justify-content-between align-items-center">
								<div className="mr-4 text-white">
									<a href="#" className="text-white mr-2"><i className="flaticon-039-shuffle"></i></a>
									<a href="#" className="text-white mr-2"><i className="flaticon-003-diamond"></i></a>
									<a href="#" className="text-white"><i className="flaticon-068-pencil"></i></a>
								</div>
								<div>
									<button className="btn btn-primary btn-sm">Aktifkan</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			)
		})
		
        return ( 
            <>
                <div className="form-head mb-sm-5 mb-3 d-flex align-items-center flex-wrap">
                    <h2 className="font-w600 mb-0 mr-auto mb-2 text-black">Dashboard</h2>
                </div>
                <div className="row">
                    <div className="col-xl-3 col-xxl-4">
                        <div className="swiper-box">
                            <div className="swiper-container card-swiper">
                                <div className="swiper-wrapper">
                                    {wallets}
                                </div>
                                <div className="swiper-scrollbar"></div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-9 col-xxl-8">
                        <div className="row">
							<div className="col-xl-12">
								<div className="d-block d-sm-flex mb-4">
									<h4 className="mb-0 text-black fs-20 mr-auto">Detail Dompet</h4>
								</div>
							</div>	
							<div className="col-xl-12">
								<div className="card">
									<div className="card-body">
										<div className="row align-items-start">
											<div className="col-xl-6 col-lg-12 col-xxl-12">
												<div className="row">
													<div className="col-sm-6">
														<div className="mb-4">
															<p className="mb-2">Nama Dompet</p>
															<h4 className="text-black">Dompet Gue</h4>
														</div>
														<div className="mb-4">
															<p className="mb-2">Dibuat Pada</p>
															<h4 className="text-black">08/21</h4>
														</div>
														<div className="mb-4">
															<p className="mb-2">Saldo</p>
															<h4 className="text-black">Rp 12.000.000</h4>
														</div>
													</div>
													<div className="col-sm-6">
														<div className="mb-4">
															<p className="mb-2">Pemasukan</p>
															<h4 className="text-black">Rp 10.000.000</h4>
														</div>
														<div className="mb-4">
															<p className="mb-2">Pengeluaran</p>
															<h4 className="text-black">Rp 10.000.000</h4>
														</div>
														<div className="mb-4">
															<p className="mb-2">Hutang</p>
															<h4 className="text-black">Rp 120.000.000</h4>
														</div>
														<div className="mb-4">
															<p className="mb-2">Piutang</p>
															<h4 className="text-black">Rp 120.000.000</h4>
														</div>
													</div>
												</div>
											</div>
											<div className="col-xl-6 col-lg-12 col-xxl-12 mb-lg-0 mb-3">
												<p>Kategori Pengeluaran</p>
												<div className="row">
													<div className="col-12 mb-sm-0 mb-4 text-center">
														<img src="/img/chart/pie.png" width="50%" />
														<h5>Pengeluaran</h5>
														{/* <div className="d-inline-block position-relative donut-chart-sale mb-3">
															<span className="donut1" data-peity='{ "fill": ["rgb(255, 104, 38)", "rgba(240, 240, 240)"],   "innerRadius": 40, "radius": 10}'>5/8</span>
															<small>66%</small>
														</div>
														<h5 className="fs-18 text-black">Main Limits</h5>
														<span>$10,000</span> */}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="col-xl-12">
								<div className="card">
									<div className="card-header d-block d-sm-flex border-0 pb-sm-0 pb-0 align-items-center">
										<div className="mr-auto mb-sm-0 mb-3">
											<h4 className="fs-20 text-black">Penghasilan & Pengeluaran</h4>
										</div>
										<select className="form-control style-1 default-select ">
											<option>Bulan Ini</option>
											<option>Bulan Lalu</option>
										</select>
									</div>
									<div className="card-body pt-3">
										<div className="flex-wrap mb-sm-4 mb-2 align-items-center">
											<div className="d-flex align-items-center">
												<span className="fs-32 text-black font-w600 pr-3">Rp 12.000.000</span>
												<span className="fs-22 text-success">7% <i className="fa fa-caret-up scale5 ml-2 text-success" aria-hidden="true"></i></span>
											</div>
											<p className="mb-0 text-black mr-auto">Pemasukan Bersih</p>
										</div>
										{/* <div id="chartTimeline" className="timeline-chart"></div> */}
										<img src="img/chart/bar.png" width="100%"/>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </>
        );
    }
}
 
export default Dashboard 