//SideBar
import { Dashboard } from './app/controllers/DashboardController'
import { Transaction } from './app/controllers/TransactionController'
import { Report } from './app/controllers/ReportController'
import { ScheduleTransaction } from './app/controllers/ScheduleTransactionController'
import { DebtReceivable } from './app/controllers/DebtReceivableController'

//NonSideBar
import { Profile } from './app/controllers/ProfileController'
import { DompetGue } from './app/controllers/DompetGueController'
import { Category } from './app/controllers/CategoryController'
import { About } from './app/controllers/AboutController'

const routes = [
    { 
        path: '/', 
        exact: true,
        name: 'Dashboard', 
        component: Dashboard,
        loading: true 
    }, 
    { 
        path: '/transaction', 
        name: 'Transaction', 
        component: Transaction,
        loading: true 
    }, 
    { 
        path: '/report', 
        name: 'Report', 
        component: Report,
        loading: true 
    }, 
    { 
        path: '/schedule-transaction', 
        name: 'ScheduleTransaction', 
        component: ScheduleTransaction,
        loading: true 
    }, 
    { 
        path: '/debt-receivable', 
        name: 'DebtReceivable', 
        component: DebtReceivable,
        loading: true 
    }, 
    { 
        path: '/profile', 
        name: 'Profile', 
        component: Profile,
        loading: true 
    }, 
    { 
        path: '/dompet-gue', 
        name: 'DompetGue', 
        component: DompetGue,
        loading: true 
    }, 
    { 
        path: '/category', 
        name: 'Category', 
        component: Category,
        loading: true 
    }, 
    { 
        path: '/about', 
        name: 'About', 
        component: About,
        loading: true 
    }, 
]

export default routes