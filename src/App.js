import React, { Component } from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import { Switch } from 'react-router-loading'
import './assets/css/style.css'
import routes from './routes'
import Navigation from './app/components/Navigation/Navigation'
import FancyRoute from './app/components/FancyRoute/FancyRoute'

const App = () => {
  return (
    <Router>
      <Navigation/>
      <div className="content-body">
        <div className="container-fluid">
        <Switch>
          {routes.map((route, i) =>
            <FancyRoute key={i} {...route} />
          )}
        </Switch>
        </div>

        <div className="pb-3"/>
      </div>
    </Router>
  );
}

export default App;
