<p align="center"><img src="https://cazh-cdn.sgp1.digitaloceanspaces.com/dompet-gue/logoDompetGue.png"></p>

# Tentang Aplikasi

Dompet Gue merupakan aplikasi pencatatan keuangan, yang bertujuan mempermudah pengguna untuk melacak seluruh keuangan mereka. Tersedia laporan keuangan, pencatatan hutang piutang dan penjadwalan transaksi. 

Web Aplikasi dompet gue dibangun menggunakan library react js dan bootstrap.

### Alamat website 
[http://xcloudy.if06sc2.xyz/xcloudy/](http://xcloudy.if06sc2.xyz/xcloudy/)

### Kontributor
* [bagusindars](https://gitlab.com/bagusindars) - **Bagus Indar Suryanto** [18102188]
* [bilscode](https://gitlab.com/bilscode) - **Billisany Akhyar** [18102225]
* [zhfiras](https://gitlab.com/18102253) - **Zhafirah** [18102253]
